from time import sleep
import hermes_py as hermes_py
from pygamepad.gamepads import Gamepad

CORE_UDP_IP = "192.168.9.43"
CORE_UDP_PORT = 42069
CORE_MOTHER_ID = 144

HERMES_UDP_PORT = 11000
HERMES_GAMEPAD_ID = 15
HERMES_UPDATE_COMMAND_ID = 0
JOYSTICK_DEADZONE = 0.2 #BELOW THIS ABSOLUTE VALUE, READS 0

print("=====================================")
print("DISCLAIMER")
print("ONLY TESTED WITH XBOX ONE CONTROLLER.")
print("IT ONLY WORKS WITH WIRED CONTROLLERS.")
print("=====================================")

gamepad = Gamepad()
gamepad.listen()

core = hermes_py.UDPClientAddr()
core.addr = CORE_UDP_IP
core.port = CORE_UDP_PORT

hermes = hermes_py.HermesUDP(HERMES_GAMEPAD_ID)
hermes.set_client(CORE_MOTHER_ID, core)

code = hermes.open(HERMES_UDP_PORT) #MY OWN PORT
if code != hermes_py.SocketReturnCode.ok:
    print("Cannot open socket: ", code)
    exit(1)

suc = hermes.start()
if not suc:
    print("Cannot start threads")
    exit(1)

announce = hermes.serialize(hermes_py.announce, hermes.hermes.id, [])
hermes.write(core, announce)

print("Up and running.")

try:
    while True:

        hermes.flush()

        buf = hermes.get()
        if buf is None:
            sleep(0.05)
            continue

        buf.rewind()

        if buf.remote != CORE_MOTHER_ID:
            print(buf.remote, " tried to talk to me.")
            continue

        if buf.command == 4095:
            continue

        if buf.command != HERMES_UPDATE_COMMAND_ID:
            print("Invalid command: ", buf.command)
            continue
        
        toSend = hermes_py.HermesBuffer()
        toSend.clear()
        toSend.set_destination(CORE_MOTHER_ID, buf.command, True)
        
        toSend.add_argument(gamepad.buttons.BTN_SOUTH.value)    #A boolean
        toSend.add_argument(gamepad.buttons.BTN_EAST.value)     #B boolean
        
        toSend.add_argument(gamepad.buttons.BTN_NORTH.value)    #X boolean
        toSend.add_argument(gamepad.buttons.BTN_WEST.value)     #Y boolean
        
        toSend.add_argument(gamepad.buttons.BTN_SELECT.value)   #SELECT boolean
        toSend.add_argument(gamepad.buttons.BTN_START.value)    #MENU boolean

        toSend.add_argument(gamepad.buttons.BTN_TR.value)       #RB boolean
        toSend.add_argument(gamepad.buttons.BTN_TL.value)       #LB boolean

        toSend.add_argument(gamepad.buttons.BTN_THUMBR.value)   #RIGHT JOYSTICK CLICK
        toSend.add_argument(gamepad.buttons.BTN_THUMBL.value)   #LEFT JOYSTICK CLICK

        toSend.add_argument(gamepad.buttons.ABS_HAT0X.value)    #CROSS X [-1, 0, +1]
        toSend.add_argument(gamepad.buttons.ABS_HAT0Y.value)    #CROSS Y [-1, 0, +1]

        


        rightJoystick = [
            gamepad.buttons.ABS_RX.value,
            gamepad.buttons.ABS_RY.value,
            gamepad.buttons.ABS_RZ.value
        ]
        rightJoystick = [(x if (abs(x) >= JOYSTICK_DEADZONE) else 0) for x in rightJoystick] #Deadzone filtering

        for x in rightJoystick:
            toSend.add_argument(x)




        leftJoystick = [
            gamepad.buttons.ABS_X.value,
            gamepad.buttons.ABS_Y.value,
            gamepad.buttons.ABS_Z.value
        ]
        leftJoystick = [(x if (abs(x) >= JOYSTICK_DEADZONE) else 0) for x in leftJoystick]  #Deadzone filtering

        for x in leftJoystick:
            toSend.add_argument(x)
        
        hermes.send(toSend)

        

except KeyboardInterrupt:
    print("Closing all threads.")
    
    gamepad.stop_listening()

    hermes.hermes.log(123, "Closing gamepad.")
    hermes.flush()
    hermes.stop()
