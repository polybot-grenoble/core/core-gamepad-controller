# Core Gamepad Peripheral
| Author: Baptiste Maheut

Small program which takes XBox One controller inputs and sends them to Core via Hermes UDP.

## How to use
0. Compile [Hermes Python](https://gitlab.com/polybot-grenoble/core/hermes-py) on your computer. If it does not work, try harder or send an email to : julien.pierson@polybot-grenoble.fr

The binary must be in the same folder than `main.py`.

1. Use Linux
2. Connect an XBox One Controller with a USB cable.

**NB:** You can try with another controller but you might have to remap some buttons. In all cases, it must be **wired** to the computer.

3. Configure `CORE_UDP_IP` and `CORE_UDP_PORT` inside `main.py`. Make sure Core's instance and your computer are on the same network.
4. run `python3 main.py` in your terminal.

**NB:** It uses bare UDP therefore it will not warn you if the network goes down.

5. Get the gamepad's inputs using the `gamepad.lua` peripheral file. An example strategy called `manual_udp.lua` uses it.

## Useful constants
The following table describes the constants of the peripheral:

|Constant|Default value|Description|
|---------|----------|----------|
|CORE_UDP_IP |"192.168.9.43" |The robot's ip address.|
|CORE_UDP_PORT | 42069 |UDP port used by the Core instance on the robot.|
|CORE_MOTHER_ID | 144| Hermes ID of Core. |
|HERMES_UDP_PORT | 11000| UDP port of the gamepad. |
|HERMES_GAMEPAD_ID | 15 | Hermes ID of the gamepad. |
|HERMES_UPDATE_COMMAND_ID | 0 | UPDATE_COMMAND ID of the gamepad. |
|JOYSTICK_DEADZONE | 0.2 | Deadzone around the gamepad's joystick to prevent drifting. |