from typing import ClassVar, Optional, overload

announce: HermesUDPOPCode
bind_fail: SocketReturnCode
buffer_none: HermesReturnCode
buffer_not_found: HermesReturnCode
buffer_too_big: HermesReturnCode
buffer_too_small: HermesReturnCode
busy: HermesErrorCode
char: HermesArgumentTypeIndicator
chunk_too_big: HermesReturnCode
chunk_too_small: HermesReturnCode
create_fail: SocketReturnCode
discover: HermesUDPOPCode
double: HermesArgumentTypeIndicator
fifo_empty: HermesReturnCode
fifo_full: HermesReturnCode
float: HermesArgumentTypeIndicator
forbidden: HermesReturnCode
int16: HermesArgumentTypeIndicator
int32: HermesArgumentTypeIndicator
int64: HermesArgumentTypeIndicator
int8: HermesArgumentTypeIndicator
invalid_arg_type: HermesReturnCode
invalid_argument: HermesErrorCode
invalid_command: HermesReturnCode
invalid_command_ID: HermesErrorCode
invalid_id: HermesReturnCode
not_opened: SocketReturnCode
ok: SocketReturnCode
payload_too_big: SocketReturnCode
read_fail: SocketReturnCode
req: HermesUDPOPCode
res: HermesUDPOPCode
string: HermesArgumentTypeIndicator
unknown: HermesErrorCode
write_fail: SocketReturnCode

class Hermes:
    chunk_length: int
    id: int
    inputs: list[HermesBuffer]
    is_sending: bool
    outputs: list[HermesBuffer]
    queue: HermesQueue
    def __init__(self) -> None:
        """__init__(self: hermes_py.Hermes) -> None"""
    def error(self, reciever: int, error: HermesErrorCode, command: int) -> None:
        """error(self: hermes_py.Hermes, reciever: int, error: hermes_py.HermesErrorCode, command: int) -> None

        Sends an error packet
        """
    def heartbeat(self, reciever: int, is_response: bool = ...) -> None:
        """heartbeat(self: hermes_py.Hermes, reciever: int, is_response: bool = False) -> None

        Sends a heartbeat packet
        """
    def init(self, id: int, chunk_length: int) -> None:
        """init(self: hermes_py.Hermes, id: int, chunk_length: int) -> None

        Initializes Hermes
        """
    def log(self, reciever: int, message: str) -> None:
        """log(self: hermes_py.Hermes, reciever: int, message: str) -> None

        Sends a log packet
        """
    def next_buffer(self) -> HermesBuffer:
        """next_buffer(self: hermes_py.Hermes) -> hermes_py.HermesBuffer

        Retrives the next input buffer to process
        """
    def output_buffer_index(self, buffer: HermesBuffer) -> int:
        """output_buffer_index(self: hermes_py.Hermes, buffer: hermes_py.HermesBuffer) -> int

        Gets the index number of an output buffer. Returns HERMES_MAX_BUFFER_LEN if not found.
        """
    def partial_begin(self, receiver: int, command: int, data_length: int, is_response: bool = ...) -> None:
        """partial_begin(self: hermes_py.Hermes, receiver: int, command: int, data_length: int, is_response: bool = False) -> None

        Sends a partial begin packet
        """
    def recieve_data(self, remote: int, command: int, data: list[int], is_response: bool = ...) -> None:
        """recieve_data(self: hermes_py.Hermes, remote: int, command: int, data: list[int], is_response: bool = False) -> None

        Tells to Hermes that data has been recieved
        """
    def send_buffer(self, index: int) -> None:
        """send_buffer(self: hermes_py.Hermes, index: int) -> None

        Adds a buffer index to the end of the send queue
        """
    def send_data(self) -> tuple:
        """send_data(self: hermes_py.Hermes) -> tuple

        Gets the next data to send and it's destination
        """
    def send_prioritized_buffer(self, index: int) -> None:
        """send_prioritized_buffer(self: hermes_py.Hermes, index: int) -> None

        Adds a buffer index to the start of the send queue
        """
    def signal(self, reciever: int, signal: int) -> None:
        """signal(self: hermes_py.Hermes, reciever: int, signal: int) -> None

        Sends a signal packet
        """
    @property
    def BUFFER_COUNT(self) -> int: ...
    @property
    def MAX_BUFFER_LEN(self) -> int: ...
    @property
    def complete_buffer_count(self) -> int: ...
    @property
    def waiting_for_TX(self) -> bool: ...

class HermesArgumentTypeIndicator:
    __members__: ClassVar[dict] = ...  # read-only
    __entries: ClassVar[dict] = ...
    char: ClassVar[HermesArgumentTypeIndicator] = ...
    double: ClassVar[HermesArgumentTypeIndicator] = ...
    float: ClassVar[HermesArgumentTypeIndicator] = ...
    int16: ClassVar[HermesArgumentTypeIndicator] = ...
    int32: ClassVar[HermesArgumentTypeIndicator] = ...
    int64: ClassVar[HermesArgumentTypeIndicator] = ...
    int8: ClassVar[HermesArgumentTypeIndicator] = ...
    string: ClassVar[HermesArgumentTypeIndicator] = ...
    def __init__(self, value: int) -> None:
        """__init__(self: hermes_py.HermesArgumentTypeIndicator, value: int) -> None"""
    def __eq__(self, other: object) -> bool:
        """__eq__(self: object, other: object) -> bool"""
    def __hash__(self) -> int:
        """__hash__(self: object) -> int"""
    def __index__(self) -> int:
        """__index__(self: hermes_py.HermesArgumentTypeIndicator) -> int"""
    def __int__(self) -> int:
        """__int__(self: hermes_py.HermesArgumentTypeIndicator) -> int"""
    def __ne__(self, other: object) -> bool:
        """__ne__(self: object, other: object) -> bool"""
    @property
    def name(self) -> str: ...
    @property
    def value(self) -> int: ...

class HermesBuffer:
    command: int
    data: list[int]
    in_use: bool
    is_response: bool
    len: int
    pos: int
    remote: int
    def __init__(self) -> None:
        """__init__(self: hermes_py.HermesBuffer) -> None"""
    @overload
    def add_argument(self, argument: int) -> None:
        """add_argument(*args, **kwargs)
        Overloaded function.

        1. add_argument(self: hermes_py.HermesBuffer, argument: int) -> None

        Adds an argument to the buffer

        2. add_argument(self: hermes_py.HermesBuffer, argument: int) -> None

        Adds an argument to the buffer

        3. add_argument(self: hermes_py.HermesBuffer, argument: int) -> None

        Adds an argument to the buffer

        4. add_argument(self: hermes_py.HermesBuffer, argument: int) -> None

        Adds an argument to the buffer

        5. add_argument(self: hermes_py.HermesBuffer, argument: float) -> None

        Adds an argument to the buffer

        6. add_argument(self: hermes_py.HermesBuffer, argument: float) -> None

        Adds an argument to the buffer

        7. add_argument(self: hermes_py.HermesBuffer, argument: str) -> None

        Adds an argument to the buffer

        8. add_argument(self: hermes_py.HermesBuffer, argument: str) -> None

        Adds an argument to the buffer
        """
    @overload
    def add_argument(self, argument: int) -> None:
        """add_argument(*args, **kwargs)
        Overloaded function.

        1. add_argument(self: hermes_py.HermesBuffer, argument: int) -> None

        Adds an argument to the buffer

        2. add_argument(self: hermes_py.HermesBuffer, argument: int) -> None

        Adds an argument to the buffer

        3. add_argument(self: hermes_py.HermesBuffer, argument: int) -> None

        Adds an argument to the buffer

        4. add_argument(self: hermes_py.HermesBuffer, argument: int) -> None

        Adds an argument to the buffer

        5. add_argument(self: hermes_py.HermesBuffer, argument: float) -> None

        Adds an argument to the buffer

        6. add_argument(self: hermes_py.HermesBuffer, argument: float) -> None

        Adds an argument to the buffer

        7. add_argument(self: hermes_py.HermesBuffer, argument: str) -> None

        Adds an argument to the buffer

        8. add_argument(self: hermes_py.HermesBuffer, argument: str) -> None

        Adds an argument to the buffer
        """
    @overload
    def add_argument(self, argument: int) -> None:
        """add_argument(*args, **kwargs)
        Overloaded function.

        1. add_argument(self: hermes_py.HermesBuffer, argument: int) -> None

        Adds an argument to the buffer

        2. add_argument(self: hermes_py.HermesBuffer, argument: int) -> None

        Adds an argument to the buffer

        3. add_argument(self: hermes_py.HermesBuffer, argument: int) -> None

        Adds an argument to the buffer

        4. add_argument(self: hermes_py.HermesBuffer, argument: int) -> None

        Adds an argument to the buffer

        5. add_argument(self: hermes_py.HermesBuffer, argument: float) -> None

        Adds an argument to the buffer

        6. add_argument(self: hermes_py.HermesBuffer, argument: float) -> None

        Adds an argument to the buffer

        7. add_argument(self: hermes_py.HermesBuffer, argument: str) -> None

        Adds an argument to the buffer

        8. add_argument(self: hermes_py.HermesBuffer, argument: str) -> None

        Adds an argument to the buffer
        """
    @overload
    def add_argument(self, argument: int) -> None:
        """add_argument(*args, **kwargs)
        Overloaded function.

        1. add_argument(self: hermes_py.HermesBuffer, argument: int) -> None

        Adds an argument to the buffer

        2. add_argument(self: hermes_py.HermesBuffer, argument: int) -> None

        Adds an argument to the buffer

        3. add_argument(self: hermes_py.HermesBuffer, argument: int) -> None

        Adds an argument to the buffer

        4. add_argument(self: hermes_py.HermesBuffer, argument: int) -> None

        Adds an argument to the buffer

        5. add_argument(self: hermes_py.HermesBuffer, argument: float) -> None

        Adds an argument to the buffer

        6. add_argument(self: hermes_py.HermesBuffer, argument: float) -> None

        Adds an argument to the buffer

        7. add_argument(self: hermes_py.HermesBuffer, argument: str) -> None

        Adds an argument to the buffer

        8. add_argument(self: hermes_py.HermesBuffer, argument: str) -> None

        Adds an argument to the buffer
        """
    @overload
    def add_argument(self, argument: float) -> None:
        """add_argument(*args, **kwargs)
        Overloaded function.

        1. add_argument(self: hermes_py.HermesBuffer, argument: int) -> None

        Adds an argument to the buffer

        2. add_argument(self: hermes_py.HermesBuffer, argument: int) -> None

        Adds an argument to the buffer

        3. add_argument(self: hermes_py.HermesBuffer, argument: int) -> None

        Adds an argument to the buffer

        4. add_argument(self: hermes_py.HermesBuffer, argument: int) -> None

        Adds an argument to the buffer

        5. add_argument(self: hermes_py.HermesBuffer, argument: float) -> None

        Adds an argument to the buffer

        6. add_argument(self: hermes_py.HermesBuffer, argument: float) -> None

        Adds an argument to the buffer

        7. add_argument(self: hermes_py.HermesBuffer, argument: str) -> None

        Adds an argument to the buffer

        8. add_argument(self: hermes_py.HermesBuffer, argument: str) -> None

        Adds an argument to the buffer
        """
    @overload
    def add_argument(self, argument: float) -> None:
        """add_argument(*args, **kwargs)
        Overloaded function.

        1. add_argument(self: hermes_py.HermesBuffer, argument: int) -> None

        Adds an argument to the buffer

        2. add_argument(self: hermes_py.HermesBuffer, argument: int) -> None

        Adds an argument to the buffer

        3. add_argument(self: hermes_py.HermesBuffer, argument: int) -> None

        Adds an argument to the buffer

        4. add_argument(self: hermes_py.HermesBuffer, argument: int) -> None

        Adds an argument to the buffer

        5. add_argument(self: hermes_py.HermesBuffer, argument: float) -> None

        Adds an argument to the buffer

        6. add_argument(self: hermes_py.HermesBuffer, argument: float) -> None

        Adds an argument to the buffer

        7. add_argument(self: hermes_py.HermesBuffer, argument: str) -> None

        Adds an argument to the buffer

        8. add_argument(self: hermes_py.HermesBuffer, argument: str) -> None

        Adds an argument to the buffer
        """
    @overload
    def add_argument(self, argument: str) -> None:
        """add_argument(*args, **kwargs)
        Overloaded function.

        1. add_argument(self: hermes_py.HermesBuffer, argument: int) -> None

        Adds an argument to the buffer

        2. add_argument(self: hermes_py.HermesBuffer, argument: int) -> None

        Adds an argument to the buffer

        3. add_argument(self: hermes_py.HermesBuffer, argument: int) -> None

        Adds an argument to the buffer

        4. add_argument(self: hermes_py.HermesBuffer, argument: int) -> None

        Adds an argument to the buffer

        5. add_argument(self: hermes_py.HermesBuffer, argument: float) -> None

        Adds an argument to the buffer

        6. add_argument(self: hermes_py.HermesBuffer, argument: float) -> None

        Adds an argument to the buffer

        7. add_argument(self: hermes_py.HermesBuffer, argument: str) -> None

        Adds an argument to the buffer

        8. add_argument(self: hermes_py.HermesBuffer, argument: str) -> None

        Adds an argument to the buffer
        """
    @overload
    def add_argument(self, argument: str) -> None:
        """add_argument(*args, **kwargs)
        Overloaded function.

        1. add_argument(self: hermes_py.HermesBuffer, argument: int) -> None

        Adds an argument to the buffer

        2. add_argument(self: hermes_py.HermesBuffer, argument: int) -> None

        Adds an argument to the buffer

        3. add_argument(self: hermes_py.HermesBuffer, argument: int) -> None

        Adds an argument to the buffer

        4. add_argument(self: hermes_py.HermesBuffer, argument: int) -> None

        Adds an argument to the buffer

        5. add_argument(self: hermes_py.HermesBuffer, argument: float) -> None

        Adds an argument to the buffer

        6. add_argument(self: hermes_py.HermesBuffer, argument: float) -> None

        Adds an argument to the buffer

        7. add_argument(self: hermes_py.HermesBuffer, argument: str) -> None

        Adds an argument to the buffer

        8. add_argument(self: hermes_py.HermesBuffer, argument: str) -> None

        Adds an argument to the buffer
        """
    def clear(self) -> None:
        """clear(self: hermes_py.HermesBuffer) -> None

        Clears the buffer
        """
    def get_char(self) -> str:
        """get_char(self: hermes_py.HermesBuffer) -> str

        Retrieves a char from the arguments
        """
    def get_double(self) -> float:
        """get_double(self: hermes_py.HermesBuffer) -> float

        Retrieves a double from the arguments
        """
    def get_float(self) -> float:
        """get_float(self: hermes_py.HermesBuffer) -> float

        Retrieves a float from the arguments
        """
    def get_int16(self) -> int:
        """get_int16(self: hermes_py.HermesBuffer) -> int

        Retrieves a int16 from the arguments
        """
    def get_int32(self) -> int:
        """get_int32(self: hermes_py.HermesBuffer) -> int

        Retrieves a int32 from the arguments
        """
    def get_int64(self) -> int:
        """get_int64(self: hermes_py.HermesBuffer) -> int

        Retrieves a int64 from the arguments
        """
    def get_int8(self) -> int:
        """get_int8(self: hermes_py.HermesBuffer) -> int

        Retrieves a int8_t from the arguments
        """
    def get_string(self) -> str:
        """get_string(self: hermes_py.HermesBuffer) -> str

        Retrieves a string from the arguments
        """
    def rewind(self) -> None:
        """rewind(self: hermes_py.HermesBuffer) -> None

        Rewinds a buffer in order to be able to read the arguments
        """
    def set_destination(self, reciever: int, command: int, is_response: bool = ...) -> None:
        """set_destination(self: hermes_py.HermesBuffer, reciever: int, command: int, is_response: bool = False) -> None

        Sets a buffer's destination
        """

class HermesErrorCode:
    __members__: ClassVar[dict] = ...  # read-only
    __entries: ClassVar[dict] = ...
    busy: ClassVar[HermesErrorCode] = ...
    invalid_argument: ClassVar[HermesErrorCode] = ...
    invalid_command_ID: ClassVar[HermesErrorCode] = ...
    unknown: ClassVar[HermesErrorCode] = ...
    def __init__(self, value: int) -> None:
        """__init__(self: hermes_py.HermesErrorCode, value: int) -> None"""
    def __eq__(self, other: object) -> bool:
        """__eq__(self: object, other: object) -> bool"""
    def __hash__(self) -> int:
        """__hash__(self: object) -> int"""
    def __index__(self) -> int:
        """__index__(self: hermes_py.HermesErrorCode) -> int"""
    def __int__(self) -> int:
        """__int__(self: hermes_py.HermesErrorCode) -> int"""
    def __ne__(self, other: object) -> bool:
        """__ne__(self: object, other: object) -> bool"""
    @property
    def name(self) -> str: ...
    @property
    def value(self) -> int: ...

class HermesQueue:
    index: list[int]
    def __init__(self) -> None:
        """__init__(self: hermes_py.HermesQueue) -> None"""
    @property
    def head(self) -> int: ...
    @property
    def tail(self) -> int: ...

class HermesReturnCode:
    __members__: ClassVar[dict] = ...  # read-only
    __entries: ClassVar[dict] = ...
    buffer_none: ClassVar[HermesReturnCode] = ...
    buffer_not_found: ClassVar[HermesReturnCode] = ...
    buffer_too_big: ClassVar[HermesReturnCode] = ...
    buffer_too_small: ClassVar[HermesReturnCode] = ...
    chunk_too_big: ClassVar[HermesReturnCode] = ...
    chunk_too_small: ClassVar[HermesReturnCode] = ...
    fifo_empty: ClassVar[HermesReturnCode] = ...
    fifo_full: ClassVar[HermesReturnCode] = ...
    forbidden: ClassVar[HermesReturnCode] = ...
    invalid_arg_type: ClassVar[HermesReturnCode] = ...
    invalid_command: ClassVar[HermesReturnCode] = ...
    invalid_id: ClassVar[HermesReturnCode] = ...
    ok: ClassVar[HermesReturnCode] = ...
    def __init__(self, value: int) -> None:
        """__init__(self: hermes_py.HermesReturnCode, value: int) -> None"""
    def __eq__(self, other: object) -> bool:
        """__eq__(self: object, other: object) -> bool"""
    def __hash__(self) -> int:
        """__hash__(self: object) -> int"""
    def __index__(self) -> int:
        """__index__(self: hermes_py.HermesReturnCode) -> int"""
    def __int__(self) -> int:
        """__int__(self: hermes_py.HermesReturnCode) -> int"""
    def __ne__(self, other: object) -> bool:
        """__ne__(self: object, other: object) -> bool"""
    @property
    def name(self) -> str: ...
    @property
    def value(self) -> int: ...

class HermesUDP(UDPSocket):
    hermes: Hermes
    @overload
    def __init__(self) -> None:
        """__init__(*args, **kwargs)
        Overloaded function.

        1. __init__(self: hermes_py.HermesUDP) -> None

        2. __init__(self: hermes_py.HermesUDP, id: int) -> None
        """
    @overload
    def __init__(self, id: int) -> None:
        """__init__(*args, **kwargs)
        Overloaded function.

        1. __init__(self: hermes_py.HermesUDP) -> None

        2. __init__(self: hermes_py.HermesUDP, id: int) -> None
        """
    def client_addr(self, id: int) -> UDPClientAddr:
        """client_addr(self: hermes_py.HermesUDP, id: int) -> hermes_py.UDPClientAddr

        Get the Client address associated to its hermesID
        """
    def client_hermesID(self, addr: UDPClientAddr) -> int:
        """client_hermesID(self: hermes_py.HermesUDP, addr: hermes_py.UDPClientAddr) -> int

        Get the Client HermesID associated to its address
        """
    def flush(self) -> None:
        """flush(self: hermes_py.HermesUDP) -> None

        Flushes the send queue
        """
    def get(self) -> Optional[HermesBuffer]:
        """get(self: hermes_py.HermesUDP) -> Optional[hermes_py.HermesBuffer]

        Get the next recieved buffer
        """
    def parse(self, data: list[int]) -> tuple:
        """parse(self: hermes_py.HermesUDP, data: list[int]) -> tuple

        Parses data from a recieved UDP buffer
        """
    def send(self, buffer: HermesBuffer) -> bool:
        """send(self: hermes_py.HermesUDP, buffer: hermes_py.HermesBuffer) -> bool

        Sends a buffer on the UDP socket
        """
    def serialize(self, op_code: HermesUDPOPCode, command: int, data: list[int]) -> list[int]:
        """serialize(self: hermes_py.HermesUDP, op_code: hermes_py.HermesUDPOPCode, command: int, data: list[int]) -> list[int]

        Serializes data to be sent over the UDP socket
        """
    @overload
    def set_client(self, id: int, addr: UDPClientAddr) -> None:
        """set_client(*args, **kwargs)
        Overloaded function.

        1. set_client(self: hermes_py.HermesUDP, id: int, addr: hermes_py.UDPClientAddr) -> None

        Associates a client Hermes ID with its address

        2. set_client(self: hermes_py.HermesUDP, id: int, host: str, port: int) -> None

        Associates a client Hermes ID with its address
        """
    @overload
    def set_client(self, id: int, host: str, port: int) -> None:
        """set_client(*args, **kwargs)
        Overloaded function.

        1. set_client(self: hermes_py.HermesUDP, id: int, addr: hermes_py.UDPClientAddr) -> None

        Associates a client Hermes ID with its address

        2. set_client(self: hermes_py.HermesUDP, id: int, host: str, port: int) -> None

        Associates a client Hermes ID with its address
        """
    def start(self) -> bool:
        """start(self: hermes_py.HermesUDP) -> bool

        Starts the HermesUDP Thread
        """
    def stop(self) -> None:
        """stop(self: hermes_py.HermesUDP) -> None

        Stops Hermes. Blocking until thread join
        """
    @property
    def last_error(self) -> SocketReturnCode: ...
    @property
    def running(self) -> bool: ...

class HermesUDPOPCode:
    __members__: ClassVar[dict] = ...  # read-only
    __entries: ClassVar[dict] = ...
    announce: ClassVar[HermesUDPOPCode] = ...
    discover: ClassVar[HermesUDPOPCode] = ...
    req: ClassVar[HermesUDPOPCode] = ...
    res: ClassVar[HermesUDPOPCode] = ...
    def __init__(self, value: int) -> None:
        """__init__(self: hermes_py.HermesUDPOPCode, value: int) -> None"""
    def __eq__(self, other: object) -> bool:
        """__eq__(self: object, other: object) -> bool"""
    def __hash__(self) -> int:
        """__hash__(self: object) -> int"""
    def __index__(self) -> int:
        """__index__(self: hermes_py.HermesUDPOPCode) -> int"""
    def __int__(self) -> int:
        """__int__(self: hermes_py.HermesUDPOPCode) -> int"""
    def __ne__(self, other: object) -> bool:
        """__ne__(self: object, other: object) -> bool"""
    @property
    def name(self) -> str: ...
    @property
    def value(self) -> int: ...

class SocketReturnCode:
    __members__: ClassVar[dict] = ...  # read-only
    __entries: ClassVar[dict] = ...
    bind_fail: ClassVar[SocketReturnCode] = ...
    create_fail: ClassVar[SocketReturnCode] = ...
    not_opened: ClassVar[SocketReturnCode] = ...
    ok: ClassVar[SocketReturnCode] = ...
    payload_too_big: ClassVar[SocketReturnCode] = ...
    read_fail: ClassVar[SocketReturnCode] = ...
    write_fail: ClassVar[SocketReturnCode] = ...
    def __init__(self, value: int) -> None:
        """__init__(self: hermes_py.SocketReturnCode, value: int) -> None"""
    def __eq__(self, other: object) -> bool:
        """__eq__(self: object, other: object) -> bool"""
    def __hash__(self) -> int:
        """__hash__(self: object) -> int"""
    def __index__(self) -> int:
        """__index__(self: hermes_py.SocketReturnCode) -> int"""
    def __int__(self) -> int:
        """__int__(self: hermes_py.SocketReturnCode) -> int"""
    def __ne__(self, other: object) -> bool:
        """__ne__(self: object, other: object) -> bool"""
    @property
    def name(self) -> str: ...
    @property
    def value(self) -> int: ...

class UDPClientAddr:
    addr: str
    port: int
    def __init__(self) -> None:
        """__init__(self: hermes_py.UDPClientAddr) -> None"""

class UDPSocket:
    port: int
    def __init__(self) -> None:
        """__init__(self: hermes_py.UDPSocket) -> None"""
    def end(self) -> None:
        """end(self: hermes_py.UDPSocket) -> None

        Closes a socket
        """
    def open(self, port: int) -> SocketReturnCode:
        """open(self: hermes_py.UDPSocket, port: int) -> hermes_py.SocketReturnCode

        Opens a socket
        """
    def read(self, arg0: UDPClientAddr, arg1: list[int]) -> SocketReturnCode:
        """read(self: hermes_py.UDPSocket, arg0: hermes_py.UDPClientAddr, arg1: list[int]) -> hermes_py.SocketReturnCode

        Reads data from the bus
        """
    def write(self, addr: UDPClientAddr, data: list[int]) -> SocketReturnCode:
        """write(self: hermes_py.UDPSocket, addr: hermes_py.UDPClientAddr, data: list[int]) -> hermes_py.SocketReturnCode

        Writes data to the interface
        """

def parse_CAN_frame_ID(id: int) -> tuple:
    """parse_CAN_frame_ID(id: int) -> tuple

    Parses a 29-bit Extended CAN ID to extract the expected informations
    """
def serialize_CAN_frame_ID(reciever: int, command: int, sender: int, is_response: bool) -> int:
    """serialize_CAN_frame_ID(reciever: int, command: int, sender: int, is_response: bool) -> int

    Serializes the informations into a 29-bit Extended CAN ID
    """
